## PARAMETERS ##
USERNAME="admin"
PASSWORD="admin"
################
# Create bucket in minio
echo "Waiting for minio to start"
sleep 10
mc alias set minio http://minio:9000 minio minio123 --api S3v4
mc mb minio/elasticsearch-backup
# Create minio repository in elasticsearch
echo "Sleeping 60, waiting for elaticsearch to initialize"
sleep 60
curl -XPUT https://odfe-node1:9200/_snapshot/minio -H 'content-type: Application/json' -u $USERNAME:$PASSWORD --insecure -d '{"type": "s3", "settings": {"bucket": "elasticsearch-backup","endpoint": "minio:9000","protocol": "http", "path_style_access": true}}'
# Create policy
curl https://odfe-node1:9200/_opendistro/_ism/policies/5d --insecure -XPUT -d "$(cat /policy_5d.json)" -uadmin:admin -H 'content-type: application/json'
# Import dashboards
curl http://kibana:5601/api/saved_objects/_import?overwrite=false --form file=@kibana.ndjson -uadmin:admin -H 'securitytenant: global' -H "kbn-xsrf: true"